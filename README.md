# Mercury Messaging System
Mercury is a server-slient messaging system designed and created by Izaak Webster (zaxn1234)

## Dependencies
- PureBasic 5.72 x64 (Should work on future versions but YMMV and I've not tested extensively on other versions)

## How to install your own server system
- Clone repo
- Build `Server/MercuryServer.pb` in PureBasic
- Run server program.
- Note the address given as your clients will need that to connect

## How to install client
- Clone repo
- Build `Client/MercuryClient.pb` in PureBasic
- Run client program
- Enter server address and authentication info to connect


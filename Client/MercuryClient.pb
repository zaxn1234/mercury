﻿EnableExplicit

;- Enumeration
Enumeration
  ;-> Window
  #Window_Main
  #Window_About
  
  ;-> Menu
  #Menu_Main
  #Menu_About
  #Menu_Exit
  
  ;-> Text
  #Text_About
  #Text_About_BuildDate
  #Text_About_Version
  
  ;-> Buttons
  #Button_About_Close
  
EndEnumeration


;- Global Variables
;-> Constants
#APP_NAME = "Mercury Client"
#APP_VERSION_MAJOR = 0
#APP_VERSION_MINOR = 0
#APP_VERSION_REVISION = 0
#APP_TITLE = #APP_NAME + " v" + #APP_VERSION_MAJOR + "." + #APP_VERSION_MINOR + "." + #APP_VERSION_REVISION
#APP_BUILD_DATE = "2021-10-19"
;-> Variables
Global Quit = 0, Event
Global DesktopW, DesktopH
;-> Procedures
Declare Inits()

;- Includes
XIncludeFile "Inc/KeyboardShortcuts.pbi"

Procedure Inits()
  ExamineDesktops()
  DesktopW = DesktopWidth(0) : DesktopH = DesktopHeight(0)
  
  ; Open Window
  OpenWindow(#Window_Main, 0, 0, DesktopW, DesktopH, #APP_TITLE, #PB_Window_ScreenCentered | #PB_Window_Maximize | #PB_Window_SystemMenu)
  
  ; Create Menu
  CreateMenu(#Menu_Main, WindowID(#Window_Main))
  MenuTitle("File")
    MenuItem(#Menu_Exit, "E&xit")
  MenuTitle("Help")
    MenuItem(#Menu_About, "About" + Chr(9) + "F1")
  
  ; Set up shortcuts
  KeyboardShortcuts()
  
EndProcedure

Procedure About()
  OpenWindow(#Window_About, 0, 0, 300, 100, "About " + #APP_NAME, #PB_Window_ScreenCentered | #PB_Window_BorderLess | #PB_Window_TitleBar, WindowID(#Window_Main))
  TextGadget(#Text_About, 10, 10, 280, 30, "Mercury is a server-slient messaging system designed and created by Izaak Webster")
  TextGadget(#Text_About_Version, 10, 60, 280, 15, "Version: " + #APP_VERSION_MAJOR + "." + #APP_VERSION_MINOR + "." + #APP_VERSION_REVISION)
  TextGadget(#Text_About_BuildDate, 10, 75, 280, 20, "Build Date: " + #APP_BUILD_DATE)
  ButtonGadget(#Button_About_Close, 250, 60, 40, 30, "Close")
EndProcedure

Procedure Exit()
  Quit = 1
EndProcedure

;- Entry Point / Hook
Inits()

; Repeat Loop
Repeat
  Event = WaitWindowEvent()
  
  Select Event
    Case #PB_Event_SizeWindow, #PB_Event_MoveWindow
      ResizeWindow(#Window_Main, 0, 0, DesktopW, DesktopH)
    Case #PB_Event_CloseWindow : Quit = 1
    Case #PB_Event_Gadget
      Select EventGadget()
          Case #Button_About_Close : CloseWindow(#Window_About)
      EndSelect
    Case #PB_Event_Menu
      Select EventMenu()
        Case #Menu_About : About()
        Case #Menu_Exit : Exit()
      EndSelect
  EndSelect
Until Quit = 1
End
; IDE Options = PureBasic 5.72 (Windows - x64)
; CursorPosition = 65
; FirstLine = 38
; Folding = -
; EnableXP